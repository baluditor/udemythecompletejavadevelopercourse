package udemyTheCompleteJavaDeveloperCourse;

public class DeluxeHamburger extends Hamburger {

    public DeluxeHamburger(String breadRollType, String meat, int basePrice) {
        super("Delux burger",breadRollType, meat, basePrice);
        super.addAdditions1("Chips", 20);
        super.addAdditions2("Cocke", 40);
    }

    @Override
    public void addAdditions1(String name, int price) {
        System.out.println("No further additions allowed");
    }

    @Override
    public void addAdditions2(String name, int priice){
        System.out.println("No further additions allowed");
    }

    @Override
    public void addAdditions3(String name, int price) {
        System.out.println("No further additions allowed");
    }

    @Override
    public void addAdditions4(String name, int price) {
        System.out.println("No further additions allowed");
    }

    @Override
    public int getTotalPrice() {
        return super.getTotalPrice();
    }

    @Override
    public int getAdditionsPrice() {
        return super.getAdditionsPrice();
    }

    @Override
    public String getAllAdditions() {
        sw.append(super.getAddition1()+", ");
        sw.append("and "+super.getAddition2());
        return sw.toString();
    }
}
