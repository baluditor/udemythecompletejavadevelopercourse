package udemyTheCompleteJavaDeveloperCourse;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String name;
    private String artist;
    private ArrayList<Song> songs = new ArrayList<>();

    public Album(String name, String artist){
        this.name = name;
        this.artist = artist;
        this.songs = new ArrayList<Song>();
    }

    public boolean addSong(String title, double duration){
        if (songExist(title) == null){
            this.songs.add(new Song(title,duration));
            return true;
        }
        return false;
    }

    private Song songExist(String title){
        for (Song checkedSong : this.songs) {
            if (checkedSong.getTitle().toLowerCase().equals(title.toLowerCase())) {
                return checkedSong;
            }
        }
        return null;
    }

    public boolean addToPlaylist(String title, LinkedList<Song> playlist){
        Song checkedSong = songExist(title);
        if (checkedSong != null) {
            playlist.add(checkedSong);
            return true;
        }
        System.out.println("The song is not on the album");
        return false;
    }


}
