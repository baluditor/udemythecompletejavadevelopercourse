package udemyTheCompleteJavaDeveloperCourse;

import java.util.Scanner;

public class BuildInventory {
    public static Inventory inventory = new Inventory();
    public static Scanner s = new Scanner(System.in);

    public static void main(String[] args ){

        startInventory();
        boolean quit = false;
        int options;

        while (!quit){
            options();
            options = s.nextInt();
            s.nextLine();

            switch (options){
                case 0:
                    quit = true;
                    break;
                case 1:
                    System.out.println("Printing inventory");
                    inventory.printInventory();
                    break;
                case 2:
                    addFrame();
                    break;
                case 3:
                    searchForFrame();
                    break;
                case 4:
                    modifyQuantity();
                    break;
                case 5:
                    deleteFrame();
                    break;
                case 6:
                    printInstructions();
                    break;
            }
        }
    }

    public static void addFrame(){
        System.out.println("Please enter if the frame is optical or sunglasses: ");
        String type = s.nextLine();
        System.out.println("Please enter the frame's brand: ");
        String brand = s.nextLine();
        System.out.println("Please enter the frame's model: ");
        String model = s.nextLine();
        System.out.println("Please enter the frame's color: ");
        String color = s.nextLine();
        System.out.println("Please enter the frame's price: ");
        int price = s.nextInt();
        System.out.println("Please enter how many pieces: ");
        int pieces = s.nextInt();
        Product product = Product.addProduct(type,brand,model,color,price,pieces);
        if (inventory.addProduct(product)){
            System.out.println("Product successfully added");
        } else {
            System.out.println("Product already exist");
        }
    }

    public static void searchForFrame(){
        System.out.println("Please enter the model name of the frame");
        String model = s.nextLine();
        Product product = inventory.queryProduct(model);
        if (product == null){
            System.out.println("No such model");
        } else {
            int modelsPosition = inventory.findProduct(model);
            modelsPosition +=1;
            System.out.println("Model found at "+modelsPosition);
        }
    }

    public static void modifyQuantity(){
        System.out.println("Please enter the model you want to modify");
        String model = s.nextLine();
        System.out.println("Please enter how many pieces you want to add");
        int pieces = s.nextInt();
        Product product = inventory.queryProduct(model);
        if (model == null){
            System.out.println("No such product");
        } else {
            inventory.modifyQuantity(product,pieces);
        }
    }

    public static void deleteFrame(){
        System.out.println("Please enter the model you want to delete");
        String model = s.nextLine();
        Product product = inventory.queryProduct(model);
        if (model == null){
            System.out.println("No such contact");
        } else {
            if (inventory.deleteProduct(product)){
                System.out.println("Product successfully deleted");
            }
        }
    }

    public static void welcomeMessage(){
        System.out.println("Inventory manager started");
    }

    public static void startInventory(){
        welcomeMessage();
        printInstructions();
    }

    public static void options(){
        System.out.println("Please enter your choice:");
    }

    public static void printInstructions(){
        System.out.println("Please choose from the following options: \n"
                +"0. Exit program\n"
                +"1 Print the inventory\n"
                +"2 Add frame\n"
                +"3 Search for frame\n"
                +"4 Modify quantity\n"
                +"5 Delete frame\n"
                +"6 Print instructions\n"
        );
    }
}
