package udemyTheCompleteJavaDeveloperCourse;

class Car{
    private String name;
    private boolean engine;
    private int cylinders;
    private int wheels;
    private int doors;
    private int windows;
    private int speed;

    public Car(String name, int cylinders){
        this.name = name;
        this.cylinders = cylinders;
        this.engine = true;
        this.wheels = 4;
        this.doors = 4;
        this.windows = 6;
        this.speed = 0;
    }

    public int getSpeed(){
        return this.speed;
    }

    public String startEngine(){
        return "The engine has been started";
    }

    public void voidStartEngine(){
        System.out.println("The voidEngine has been started");
    }

    public int accelerate(int speed){
        System.out.println("The Car has used accelerate function now its speed is "+this.speed);
        return this.speed += speed;
    }

    public int brake(int force){
        System.out.println("The Car has used brake function now its speed is "+this.speed);
        return this.speed -= force;
    }

    public String getName(){
        return this.name;
    }

    public boolean haveEngine(){
        return this.engine;
    }

    public int getCylinders() {
        return cylinders;
    }

    public int getWheels() {
        return wheels;
    }

    public int getDoors() {
        return doors;
    }

    public int getWindows() {
        return windows;
    }
}

class Honda extends Car{
    public Honda() {
        super("Honda",16);
    }

    @Override
    public int accelerate(int speed) {
        System.out.println("The honda is accelerating with "+speed+" and the its cucrent speed is "+super.getSpeed());
        return super.accelerate(speed);
    }

    @Override
    public int brake(int force){
        int currentSpeed = super.getSpeed()-super.brake(force);
        System.out.println("The honda is accelerating with "+force+" and the its cucrent speed is "+currentSpeed);
        return super.brake(force);
    }

    @Override
    public int getCylinders(){
        return super.getCylinders();
    }

    @Override
    public String getName(){
        return super.getName();
    }
}

class Opel extends Car{
    public Opel() {
        super("Opel",14);
    }

    @Override
    public int accelerate(int speed) {
        System.out.println("The honda is accelerating with "+speed+" and the its cucrent speed is "+super.getSpeed());
        return super.accelerate(speed);
    }

    @Override
    public int brake(int force){
        int currentSpeed = super.getSpeed()-super.brake(force);
        System.out.println("The honda is accelerating with "+force+" and the its cucrent speed is "+currentSpeed);
        return super.brake(force);
    }

    @Override
    public int getCylinders(){
        return super.getCylinders();
    }

    @Override
    public String getName(){
        return super.getName();
    }
}

class Subaru extends Car{
    public Subaru() {
        super("Subaru",18);
    }

    @Override
    public int accelerate(int speed) {
        System.out.println("The honda is accelerating with "+speed+" and the its cucrent speed is "+super.getSpeed());
        return super.accelerate(speed);
    }

    @Override
    public int brake(int force){
        int currentSpeed = super.getSpeed()-super.brake(force);
        System.out.println("The honda is accelerating with "+force+" and the its cucrent speed is "+currentSpeed);
        return super.brake(force);
    }

    @Override
    public int getCylinders(){
        return super.getCylinders();
    }

    @Override
    public String getName(){
        return super.getName();
    }
}

public class polymorphismChallengeRunner{

    public static void main( String[] args )
    {
        for (int i=0; i<11;i++){
            Car car = randomCar();
            System.out.println("The "+i+"# car was "+car.getName()+" whit "+car.getCylinders()+" cylinders");
            System.out.println(car.accelerate(10));
            System.out.println(car.brake(10));
        }
    }

    public static Car randomCar(){
        int randomNumber = (int) (Math.random()*3)+1;
        System.out.println("The random number generated was "+randomNumber);
        switch (randomNumber){
            case 1:
                return new Honda();
            case 2:
                return new Opel();
            case 3:
                return new Subaru();
        }
        return null;
    }
}
