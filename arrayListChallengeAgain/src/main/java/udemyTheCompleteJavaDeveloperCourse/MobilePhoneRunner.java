package udemyTheCompleteJavaDeveloperCourse;

import java.util.Scanner;

public class MobilePhoneRunner {
    public static Scanner s = new Scanner(System.in);
    public static MobilePhone mobilePhone = new MobilePhone();

    public static void main(String[] args) {

        startPhone();
        options();
        boolean quit = false;
        int option;

        while (!quit) {
            System.out.println("Please enter your choice of number (6 to print the menu): ");
            Scanner s = new Scanner(System.in);
            option = s.nextInt();

            switch (option) {
                case 0:
                    System.out.println("Phone is turning off");
                    quit = true;
                    break;
                case 1:
                    mobilePhone.printContacts();
                    break;
                case 2:
                    addContact();
                    break;
                case 3:
                    queryContact();
                    break;
                case 4:
                    updateContact();
                    break;
                case 5:
                    deleteContact();
                    break;
                case 6:
                    options();
                    break;
            }
        }
    }

    public static void startPhone() {
        welcomeMessage();
    }

    public static void welcomeMessage() {
        System.out.println("The phone has been started");
    }

    public static void options() {
        System.out.println("Please type one of the options below:\n" +
                "0 to quite\n" + "" +
                "1 to print the phone book\n" +
                "2 to add a contact\n" +
                "3 to query a contact\n" +
                "4 to update a contact\n" +
                "5 to delete a contact\n" +
                "6 to print the options again\n");
    }

    public static void addContact() {
        System.out.println("Please enter the name of the contact you want to add: ");
        String name = s.next();
        System.out.println("Please enter the phone number of \n" + name);
        String phoneNumber = s.next();
        mobilePhone.addContact(new Contact(name, phoneNumber));
        System.out.println("Contact " + name + " with phone number of " + phoneNumber + " has been added successfully");
    }

    public static void queryContact() {
        System.out.println("Please enter the name of the contact you looking for: ");
        String name = s.next();
        mobilePhone.queryContact(name);
    }

    public static void updateContact() {
        System.out.println("Please enter the name of the contact you want to update: ");
        String oldName = s.next();
        System.out.println("Please enter the new name of the contact: ");
        String newName = s.next();
        System.out.println("Please enter the new number of the contact: ");
        String newPhoneNumber = s.next();
        mobilePhone.modifyContact(oldName, new Contact(newName, newPhoneNumber));
    }

    public static void deleteContact() {
        System.out.println("Please enter the name of the contact you want to delete: ");
        String name = s.next();
        mobilePhone.deleteContact(name);
    }
}
