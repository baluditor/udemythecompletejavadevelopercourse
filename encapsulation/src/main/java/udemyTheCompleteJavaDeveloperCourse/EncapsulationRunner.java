package udemyTheCompleteJavaDeveloperCourse;

public class EncapsulationRunner {

    public static void main( String[] args ){

        EnchantedPlayer player = new EnchantedPlayer("Balu", 99,"knife");
        System.out.println("The initial health is "+player.getHealth()+", using "+player.announceWeapon()+" as a weapon");

        EnchantedPlayer player1 = new EnchantedPlayer("Jakab","Fist");
        System.out.println("The initial heath is "+player1.getHealth()+", using "+player1.announceWeapon()+" as a weapon");

    }
}
