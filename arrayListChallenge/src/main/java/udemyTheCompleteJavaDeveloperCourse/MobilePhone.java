package udemyTheCompleteJavaDeveloperCourse;

import java.util.ArrayList;

public class MobilePhone {

    private String myNumber;
    private ArrayList<ContactOldie> myContactOldies;

    public MobilePhone(String myNumber) {
        this.myNumber = myNumber;
        this.myContactOldies = new ArrayList<ContactOldie>();
    }

    public boolean addNewContact(ContactOldie contactOldie){
        if(findContact(contactOldie.getName()) >=0){
            System.out.println("ContactOldie is already on file");
            return false;
        }
        myContactOldies.add(contactOldie);
        return true;
    }

    public void printContacts(){
        System.out.println("ContactOldie list");
        for (int i = 0; i<this.myContactOldies.size(); i++){
            System.out.println((i+1) + ". "+this.myContactOldies.get(i).getName()+": "+this.myContactOldies.get(i).getPhoneNumber());
        }

    }

    public boolean updateContact(ContactOldie oldContactOldie, ContactOldie newContactOldie){
        int foundPosition = findContact(oldContactOldie);
        if (foundPosition<0){
            System.out.println(oldContactOldie.getName()+" was not found");
            return false;
        }
        this.myContactOldies.set(foundPosition, newContactOldie);
        System.out.println(oldContactOldie.getName()+" was replaced with "+ newContactOldie.getName());
        return true;
    }

    public boolean removeContact(ContactOldie contactOldie){
        int foundPosition = findContact(contactOldie);
        if (foundPosition<0){
            System.out.println(contactOldie.getName()+" was not found");
            return false;
        }
        this.myContactOldies.remove(foundPosition);
        System.out.println(contactOldie.getName()+" was deleted");
        return true;
    }

    private int findContact(ContactOldie contactOldie){
        return this.myContactOldies.indexOf(contactOldie);
    }

    private int findContact(String contactName){
        for (int i = 0; i<this.myContactOldies.size(); i++){
            ContactOldie contactOldie = this.myContactOldies.get(i);
            if (contactOldie.getName().equals(contactName)){
                return i;
            }
        }
        return -1;
    }

    public String queryContact(ContactOldie contactOldie){
        if (findContact(contactOldie)>=0){
            return contactOldie.getName();
        }
        return null;
    }

    public ContactOldie queryContact(String name){
        int position = findContact(name);
        if (position>=0){
            return this.myContactOldies.get(position);
        }
        return null;
    }

}
