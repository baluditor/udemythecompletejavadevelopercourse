package udemyTheCompleteJavaDeveloperCourse;

import java.util.ArrayList;

public class MobilePhone {
    private ArrayList<Contact> myContacts;

    public MobilePhone() {
        this.myContacts = new ArrayList<>();    }

    public void printContacts() {
        for (int i = 0; i < myContacts.size(); i++) {
            System.out.println((i + 1) + ". Contact:\n" + "Name: " + myContacts.get(i).getName() + "\nPhone number: " + myContacts.get(i).getPhoneNumber()+"\n");
        }
    }

    public void addContact(Contact contact) {
        if(isContactExist(contact.getName())>-1){
            System.out.println("Can't add contact with " + contact.getName() + " name, it's already exist!");
        } else {
            myContacts.add(contact);
        }
    }

    public void queryContact(String name) {
        if (isContactExist(name)>-1){
            System.out.println(name + " contact exist and it's position is "+((isContactExist(name))+1));
        } else {
            System.out.println("Contact could not be found");
        }
    }

    public void modifyContact(String oldContact, Contact contact){
        if (isContactExist(oldContact)>-1){
            myContacts.set(isContactExist(oldContact),contact);
        } else {
            System.out.println("No such contact");
        }
    }

    public void deleteContact(String name) {
        if (isContactExist(name)>-1){
            myContacts.remove(isContactExist(name));
            System.out.println("Contact "+name +" has been removed\n");
        } else {
            System.out.println("No such contact");
        }
    }

    private int isContactExist(String name) {
        for (int i = 0; i < myContacts.size(); i++) {
            if (myContacts.get(i).getName().toLowerCase().equals(name.toLowerCase())) {
                return i;
            }
        }
        return -1;
    }
}
