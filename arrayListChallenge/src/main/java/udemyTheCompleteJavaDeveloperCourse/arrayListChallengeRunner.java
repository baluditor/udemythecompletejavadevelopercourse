package udemyTheCompleteJavaDeveloperCourse;

import java.util.Scanner;

public class arrayListChallengeRunner{
    private static Scanner scanner = new Scanner(System.in);
    private static MobilePhone mobilePhone = new MobilePhone("+420 123 132");
    public static void main( String[] args ){

        boolean quit = false;
        startPhone();
        printActions();
        while (!quit){
            System.out.println("\nEnter action: (Enter 6 to show available actions)");
            int action = scanner.nextInt();
            scanner.nextLine();

            switch (action){
                case 0:
                    System.out.println("\n Shutting down...");
                    quit = true;
                    break;
                case 1:
                    mobilePhone.printContacts();
                    break;
                case 2:
                    addNewContact();
                    break;
                case 3:
                    updateContact();
                    break;
                case 4:
                    removeContact();
                    break;
                case 5:
                    queryContact();
                    break;
                case 6:
                    printActions();
                    break;
            }
        }
    }

    private static void addNewContact(){
        System.out.println("Enter new contact name");
        String name = scanner.nextLine();
        System.out.println("Enter phone number");
        String phoneNumber = scanner.nextLine();
        //ContactOldie newContactOldie = ContactOldie.createContact(name,phoneNumber);
        ContactOldie newContactOldie = new ContactOldie(name,phoneNumber);
        if (mobilePhone.addNewContact(newContactOldie)){
            System.out.println("New contact with a name: "+name+" with phone number: "+phoneNumber+" added");
        } else {
            System.out.println("The contact already exists");
        }
    }

    private static void updateContact(){
        System.out.println("Enter an existing contact name: ");
        String name = scanner.nextLine();
        ContactOldie existingContactOldieRecord = mobilePhone.queryContact(name);
        if (existingContactOldieRecord == null){
            System.out.println("ContactOldie not found");
        }
        System.out.println("Enter new contact name: ");
        String newName = scanner.nextLine();
        System.out.println("Enter new contact phone number");
        String newNumber = scanner.nextLine();
        ContactOldie newContactOldie = ContactOldie.createContact(newName,newNumber);
        if(mobilePhone.updateContact(existingContactOldieRecord, newContactOldie)){
            System.out.println("Successfully updated the record");
        } else {
            System.out.println("Error updating record");
        }
    }

    private static void removeContact(){
        System.out.println("Enter an existing contact name: ");
        String name = scanner.nextLine();
        ContactOldie existingContactOldieRecord = mobilePhone.queryContact(name);
        if (existingContactOldieRecord == null){
            System.out.println("ContactOldie not found");
        }
        if(mobilePhone.removeContact(existingContactOldieRecord)){
            System.out.println("Successfully deleted");
        } else {
            System.out.println("Error, could not delete it");
        }
    }

    public static void queryContact(){
        System.out.println("Enter an existing contact name: ");
        String name = scanner.nextLine();
        ContactOldie existingContactOldieRecord = mobilePhone.queryContact(name);
        if (existingContactOldieRecord == null){
            System.out.println("ContactOldie not found");
            return;
        }
        System.out.println("Name "+ existingContactOldieRecord.getName() + " phone number is "+ existingContactOldieRecord.getPhoneNumber());
    }


    private static void startPhone(){
        System.out.println("Phone has started fine");
    }

    private static void printActions(){
        System.out.println("\nAvailable actions: \npress:");
        System.out.println("\n0 - to shutdown\n"
                +"1 - to print contacts\n"
                +"2 - to add new contact\n"
                +"3 - to update existing contact\n"
                +"4 - to remove existing contact\n"
                +"5 - query if the contact exists\n"
                +"6 - to print a lost of available actions");
        System.out.println("Chose you action: ");
    }
}
