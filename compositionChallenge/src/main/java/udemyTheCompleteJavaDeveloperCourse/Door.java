package udemyTheCompleteJavaDeveloperCourse;

public class Door {
    private Dimensions dimensions;
    private PhysicalAttributes physicalAttributes;
    private boolean isOpen;

    public Door(Dimensions dimensions, PhysicalAttributes physicalAttributes) {
        this.dimensions = dimensions;
        this.physicalAttributes = physicalAttributes;
        this.isOpen = false;
    }

    public void closeDoor(){
        isOpen = false;
        System.out.println("The door has been closed");
    }
    public void openDoor(){
        isOpen = true;
        System.out.println("The door has been opened");
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public PhysicalAttributes getPhysicalAttributes() {
        return physicalAttributes;
    }

    public boolean isOpen() {
        return isOpen;
    }
}
