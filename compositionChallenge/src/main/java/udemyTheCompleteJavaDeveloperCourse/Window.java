package udemyTheCompleteJavaDeveloperCourse;

public class Window {
    private Dimensions dimensions;
    private PhysicalAttributes physicalAttributes;
    private boolean isOpen;

    public Window(Dimensions dimensions, PhysicalAttributes physicalAttributes) {
        this.dimensions = dimensions;
        this.physicalAttributes = physicalAttributes;
        this.isOpen = false;
    }

    public void chageWindowstatus(){
        if (isOpen){
            closeWindow();
            System.out.println("The windows has been closed");
        } else {
            openWindow();
            System.out.println("The window has been opened");
        }
    }

    private void closeWindow(){
        isOpen = false;
    }

    private void openWindow(){
        isOpen = true;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public PhysicalAttributes getPhysicalAttributes() {
        return physicalAttributes;
    }

    public boolean isOpen() {
        return isOpen;
    }

}
