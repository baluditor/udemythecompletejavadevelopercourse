package udemyTheCompleteJavaDeveloperCourse;

public class Product {
    private String type;
    private String brand;
    private String model;
    private String color;
    private int price;
    private int piece;

    public Product(String type, String brand, String model, String color, int price, int piece) {
        this.type = type;
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.price = price;
        this.piece = piece;
    }

    public static Product addProduct(String type, String brand, String model, String color, int price, int piece){
        return new Product(type,brand,model,color,price,piece);
    }

    public void addPieces(int pieces){
        this.piece+=pieces;
    }

    public String getType() {
        return type;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public int getPrice() {
        return price;
    }

    public int getPiece() {
        return piece;
    }
}
