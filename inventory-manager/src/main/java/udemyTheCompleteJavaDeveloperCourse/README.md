Create an application which manages an inventory of products.
Create a **_product class_** which has a price, id, and quantity on hand.
Then create an _**inventory**_ class which keeps track of various products and can sum up the inventory value.