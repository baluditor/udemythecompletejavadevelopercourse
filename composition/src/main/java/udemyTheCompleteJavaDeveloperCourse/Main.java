package udemyTheCompleteJavaDeveloperCourse;

public class Main
{
    public static void main( String[] args ){
        Dimensions dimensions = new Dimensions(20,20,5);
        Case theCase = new Case("220b", "Dell",220,dimensions);

        Monitor monitor = new Monitor("27 each best", "Dell", 27, new Resolution(2540,1440));
        Motherboard motherboard = new Motherboard("BJ299", "Asus",4,10,"AMI");

        Pc pc = new Pc(theCase,monitor,motherboard);

        pc.powerUp();

        monitor.drawPixelAt(5,5,"red");

        System.out.println(pc.publicgetMotherboard().getModel());

    }
}
