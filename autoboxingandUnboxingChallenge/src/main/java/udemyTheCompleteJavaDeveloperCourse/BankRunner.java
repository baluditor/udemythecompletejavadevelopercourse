package udemyTheCompleteJavaDeveloperCourse;

/**
 * For Tim Buchalka's Complete Java Masterclass
 * https://www.udemy.com/java-the-complete-java-developer-course
**/

import java.util.Scanner;

public class BankRunner {

    private static Scanner s = new Scanner(System.in);
    private static Bank bank = new Bank("Balu's bank");

    public static void main(String[] args) {

        boolean quit = false;

        startBank();
        instruction();

        while (!quit) {
            int option;
            do {
                System.out.println("Please enter a number between 0 and 6!");
                while (!s.hasNextInt()) {
                    System.out.println("That's not a number!\nPlease enter a number between 0 and 6!");
                    s.next();
                }
                option = s.nextInt();
            } while (option < 0);
                s.nextLine();

                switch (option) {
                    case 0:
                        System.out.println("Exiting program");
                        quit = true;
                        break;
                    case 1:
                        addBranch();
                        break;
                    case 2:
                        listBranches();
                        break;
                    case 3:
                        addCustomerToBranch();
                        break;
                    case 4:
                        addTransactionToCustomer();
                        break;
                    case 5:
                        listCustomersForBranch();
                        break;
                    case 6:
                        instruction();
            }
        }
    }

    private static void addBranch() {
        System.out.print("Enter the name of the branch: \n");
        String branchName = s.nextLine();
        Branch newBranch = new Branch(branchName);
        bank.addNewBranch(newBranch);
    }

    private static void listBranches() {
        bank.listBranchesNames();
    }

    private static void addCustomerToBranch() {
        System.out.println("Name of the customer: ");
        String customerName = s.nextLine();
        System.out.println("Please specify the initial transaction amount: ");
        double initialTransaction = s.nextDouble();
        s.nextLine();
        System.out.println("Specify which branch you want to add the customer: ");
        String branchName = s.nextLine();
        bank.addNewCustomerToBranch(branchName, customerName, initialTransaction);
    }

    private static void addTransactionToCustomer() {
        System.out.println("Specify which branch name: ");
        String branchName = s.nextLine();
        System.out.println("Name of the customer: ");
        String customerName = s.nextLine();
        System.out.println("Please specify the amount: ");
        double amount = s.nextDouble();
        bank.addTransactionToCustomer(branchName, customerName, amount);
    }

    private static void listCustomersForBranch() {
        System.out.println("Please enter which branch's customer you want to see: ");
        String branchName = s.nextLine();
        bank.printCustomersInBranchWithTheirTransactions(branchName);
    }

    private static void instruction() {
        System.out.println("0 . quit" +
                "\n1. for add Branch" +
                "\n2. for list branches" +
                "\n3. for add Customer to a branch" +
                "\n4. for add transaction for customer" +
                "\n5. for list customer in a branch" +
                "\n6. for print instructions");
    }

    private static void startBank() {
        System.out.println("Bank app has been loaded." +
                "\nPlease choose from the options below: \n");
    }
}



