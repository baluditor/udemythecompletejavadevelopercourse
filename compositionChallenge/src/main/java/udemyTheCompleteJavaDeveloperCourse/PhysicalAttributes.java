package udemyTheCompleteJavaDeveloperCourse;

public class PhysicalAttributes {
    private String materialMadeOf;
    private String color;
    private int weight;

    public PhysicalAttributes(String materialMadeOf, String color, int weight) {
        this.materialMadeOf = materialMadeOf;
        this.color = color;
        this.weight = weight;
    }

    public String getMaterialMadeOf() {
        return materialMadeOf;
    }

    public String getColor() {
        return color;
    }

    public int getWeight() {
        return weight;
    }
}
