package udemyTheCompleteJavaDeveloperCourse;

public class HealthyBurger extends Hamburger {
    private String addition5;
    private String addition6;
    private int additionsPrice = 0;

    public HealthyBurger(String meat, int basePrice) {
        super("Healthy Burger","brown rye bread roll", meat, basePrice);
    }

    public void addAdditions5(String name, int price){
        this.addition5 = name;
        additionsPrice += price;
    }
    public void addAdditions6(String name, int price){
        this.addition6 = name;
        additionsPrice += price;
    }

    @Override
    public void getBasePrice(){
        super.getBasePrice();
    }

    @Override
    public int getTotalPrice() {
        return super.getTotalPrice()+this.additionsPrice;
    }

    @Override
    public String getAllAdditions() {
        sw.append(super.getAddition1()+", ");
        sw.append(super.getAddition2()+", ");
        sw.append(super.getAddition3()+", ");
        sw.append(super.getAddition4()+", ");
        sw.append(this.addition5+", ");
        sw.append("and "+this.addition6);
        return sw.toString();
    }
}
