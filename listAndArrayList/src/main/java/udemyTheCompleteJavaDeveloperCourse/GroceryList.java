package udemyTheCompleteJavaDeveloperCourse;

import java.util.ArrayList;

public class GroceryList {

    private   ArrayList<String> groceryList = new ArrayList<>();

    public ArrayList<String> getGroceryList() {
        return groceryList;
    }

    public  void addGroceryItem(String itemName){
        groceryList.add(itemName);
    }

    public void printGroceryList(){
        System.out.println("You have "+ groceryList.size()+" items in your grocery list");
        for (int i=0; i<groceryList.size(); i++){
            System.out.println(i+1+". "+groceryList.get(i));
        }
    }

    public  void modifyGroceryItem(int position, String newItem){
        groceryList.set(position-1, newItem);
        System.out.println("Grocery item "+(position+1) + "has been modified to "+newItem);
    }

    private void modifyGroceryItem(String newItem){
        int position = searchGroceryItem(newItem);
        if (position >= 0){
            modifyGroceryItem(position, newItem);
        }
    }

    public void removeGroceryItem (String item){
        int position = searchGroceryItem(item);
        if (position >= 0){
            removeGroceryItem(position);
        }
    }

    private void removeGroceryItem(int position){
        groceryList.remove(position);
    }

    private int searchGroceryItem(String searchItem){
        return groceryList.indexOf(searchItem);
    }

    public boolean onFile(String searchItem){
        int position = searchGroceryItem(searchItem);
        if (position >=0){
            return true;
        }
        return false;
    }
}
