package udemyTheCompleteJavaDeveloperCourse;

public class encapsulationChallengeRunner {
    public static void main( String[] args ){


        Printer printer = new Printer(true);
        Printer printer1= new Printer(true);

        printer.fillToner(10);
        printer1.fillToner(20);

        printer.print(5,true);
        printer.print(5,false);
        printer.print(55,false);
        printer.print(25,true);
        printer.print(15,false);
        printer.print(5,true);
        printer.fillToner(510);
        printer.print(15,false);


    }
}
