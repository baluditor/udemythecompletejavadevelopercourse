package udemyTheCompleteJavaDeveloperCourse;

import java.io.StringWriter;

public class Hamburger {
    private String breadRollType;
    private String meat;
    private String addition1;
    private String addition2;
    private String addition3;
    private String addition4;
    private int basePrice;
    private int totalPrice = 0;
    private int additionsPrice = 0;

    public Hamburger(String name,String breadRollType,String meat,int basePrice){
        this.breadRollType = breadRollType;
        this.meat = meat;
        this.basePrice = basePrice;
    }

    public void addAdditions1(String name, int price){
        this.addition1 = name;
        this.additionsPrice += price;
    }

    public void addAdditions2(String name, int price){
        this.addition2 = name;
        this.additionsPrice += price;
    }

    public void addAdditions3(String name, int price){
        this.addition3 = name;
        this.additionsPrice += price;
    }

    public void addAdditions4(String name, int price){
        this.addition4 = name;
        this.additionsPrice += price;
    }

    public int getAdditionsPrice(){
        return this.additionsPrice;
    }

    public int getTotalPrice(){
        this.totalPrice = this.basePrice+this.additionsPrice;
        return totalPrice;
    }

    public String printTotalprice(){
        return "The total price of the burger is "+this.totalPrice;
    }

    public void getBasePrice(){
        System.out.println("The base price of the burger was "+ basePrice);
    }

    public String getAddition1() {
        return addition1;
    }

    public String getAddition2() {
        return addition2;
    }

    public String getAddition3() {
        return addition3;
    }

    public String getAddition4() {
        return addition4;
    }

    StringWriter sw = new StringWriter();

    public String getAllAdditions(){
        sw.append(this.addition1+", ");
        sw.append(this.addition2+", ");
        sw.append(this.addition3+", ");
        sw.append("and "+this.addition4);
        return sw.toString();
    }
}
