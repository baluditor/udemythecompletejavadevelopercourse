package udemyTheCompleteJavaDeveloperCourse;

public abstract class Vehicle {

    private int weight;
    private int height;
    private int width;
    private int lenght;
    private String color;

    Vehicle(){

    }

    Vehicle(int weight, int height, int width, int length, String color){
        this.weight = weight;
        this.height = height;
        this.width = width;
        this.lenght = length;
        this.color = color;
    }

    public int getWeight() {
        return weight;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getLenght() {
        return lenght;
    }

    public String getColor() {
        return color;
    }
}
