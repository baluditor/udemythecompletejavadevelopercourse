package udemyTheCompleteJavaDeveloperCourse;

/**
 * Hello world!
 *
 */
public class Main
{
    public static void main( String[] args )
    {
       Wall wall1 = new Wall(new Dimensions(50,50,5),new PhysicalAttributes("Brick","green",500));
       Wall wall2 = new Wall(new Dimensions(50,50,5),new PhysicalAttributes("Brick","green",500));
       Wall wall3 = new Wall(new Dimensions(50,50,5),new PhysicalAttributes("Brick","green",500));
       Wall wall4 = new Wall(new Dimensions(50,50,5),new PhysicalAttributes("Brick","green",500));
       Wall floor = new Wall(new Dimensions(50,50,5),new PhysicalAttributes("Brick","green",500));
       Wall ceiling = new Wall(new Dimensions(50,50,5),new PhysicalAttributes("Brick","green",500));

       Window window = new Window(new Dimensions(20,20,5),new PhysicalAttributes("wood","white",20));

       Dimensions dimensions = new Dimensions(50,50,30);

       Lamp lamp = new Lamp(new Dimensions(3,20,2),new PhysicalAttributes("mixed","black",10));

       Door door = new Door(new Dimensions(10,30,5),new PhysicalAttributes("wood","wood",10));

       Room room = new Room(wall1,wall2,wall3,wall4, floor,ceiling,window,dimensions,lamp,door);

       room.getLamp().isOn();
       room.getLamp().turnOnLamp();
       room.getDoor().closeDoor();
       room.getDoor().openDoor();
       room.windowStatusChanger();
       room.windowStatusChanger();
    }
}
