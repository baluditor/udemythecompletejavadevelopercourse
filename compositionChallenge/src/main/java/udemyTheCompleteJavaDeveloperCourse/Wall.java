package udemyTheCompleteJavaDeveloperCourse;

public class Wall {
    private Dimensions dimensions;
    private PhysicalAttributes physicalAttributes;

    public Wall(Dimensions dimensions, PhysicalAttributes physicalAttributes) {
        this.dimensions = dimensions;
        this.physicalAttributes = physicalAttributes;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public PhysicalAttributes getPhysicalAttributes() {
        return physicalAttributes;
    }


}
