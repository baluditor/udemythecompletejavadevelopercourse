package udemyTheCompleteJavaDeveloperCourse;

public class FillArrayWithFor {

    public static void main( String[] args ) {

        int[][] arrayFilledWithWithForLoop = new int[10][10];

        int i;
        int j;

        for (i=0;i<arrayFilledWithWithForLoop.length;i++){
            arrayFilledWithWithForLoop[i][0] = i;
            for (j=0;j<arrayFilledWithWithForLoop.length;j++){
                arrayFilledWithWithForLoop[i][j] = j;
            }
        }

        System.out.println(arrayFilledWithWithForLoop[1][2]);
        System.out.println(arrayFilledWithWithForLoop[2][2]);



        //for (i=0; i<arrayFilledWithWithForLoop.length;i++);

    }
}
