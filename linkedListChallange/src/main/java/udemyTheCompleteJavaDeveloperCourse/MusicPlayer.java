package udemyTheCompleteJavaDeveloperCourse;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Hello world!
 *
 */
public class MusicPlayer
{
    private static ArrayList<Album> albums = new ArrayList<>();

    public static void main( String[] args )
    {
        Album album = new Album("Stormbringer", "Deep Purple");
        album.addSong("First track", 3.5);
        album.addSong("Second track", 2.7);
        album.addSong("Third track", 1.3);
        album.addSong("Forth track", 3.1);
        album.addSong("Fifth track", 5.1);
        album.addSong("Six track", 3.1);
        album.addSong("Seven track", 4.1);
        albums.add(album);

        album = new Album("Lost in space", "Talamasca");
        album.addSong("1 track", 3.5);
        album.addSong("2 track", 2.7);
        album.addSong("3 track", 1.3);
        album.addSong("4 track", 3.1);
        album.addSong("4 track", 5.1);
        album.addSong("6 track", 3.1);
        album.addSong("7 track", 4.1);
        albums.add(album);

        LinkedList<Song> playlist = new LinkedList<>();
        albums.get(0).addToPlaylist("First track", playlist);
        albums.get(0).addToPlaylist("Six track", playlist);
        albums.get(0).addToPlaylist("Seven track", playlist);
        albums.get(1).addToPlaylist("1 track", playlist);
        albums.get(1).addToPlaylist("3 track", playlist);
        albums.get(1).addToPlaylist("4 track", playlist);

        play(playlist);

    }

    private static void play(LinkedList<Song> playlist) {
        ListIterator<Song> listIterator = playlist.listIterator();
        if (playlist.size() == 0) {
            System.out.println("No song in the playlist");
        } else {
            System.out.println("Now playing: "+listIterator.next().toString());
        }
    }
}
