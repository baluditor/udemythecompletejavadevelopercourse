package udemyTheCompleteJavaDeveloperCourse;

public class Motherboard {
    private String model;
    private String motherfacturer;
    private int ramSlots;
    private int cardSlots;
    private String bios;

    public Motherboard(){

    }

    public Motherboard(String model, String motherfacturer, int ramSlots, int cardSlots, String bios) {
        this.model = model;
        this.motherfacturer = motherfacturer;
        this.ramSlots = ramSlots;
        this.cardSlots = cardSlots;
        this.bios = bios;
    }

    public void loadProgram(String programName){
        System.out.println("The program "+programName+" is loading");
    }

    public String getModel() {
        return model;
    }

    public String getMotherfacturer() {
        return motherfacturer;
    }

    public int getRamSlots() {
        return ramSlots;
    }

    public int getCardSlots() {
        return cardSlots;
    }

    public String getBios() {
        return bios;
    }
}
