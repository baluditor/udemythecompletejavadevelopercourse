package udemyTheCompleteJavaDeveloperCourse;

import java.util.Scanner;

public class GroceryListRunnerAgain {

    public static GroceryListAgain groceryList = new GroceryListAgain();
    public static Scanner s = new Scanner(System.in);

    public static void main(String[] args) {

        boolean quit = false;
        int choice;
        printInstruction();

        while (!quit){
            System.out.println("Please enter your choice:");
            choice = s.nextInt();
            s.nextLine();

            switch (choice){
                case 0:
                    printInstruction();
                    break;
                case 1:
                    groceryList.printGroceryList();
                    break;
                case 2:
                    addItem();
                    break;
                case 3:
                    queryItem();
                    break;
                case 4:
                    modifyItem();
                    break;
                case 5:
                    deleteItem();
                    break;
                case 6:
                    quit = true;
                    System.out.println("Exiting the program");
                    break;
            }
        }
    }

    public static void printInstruction(){
        System.out.println("\n Press:");
        System.out.println("\t 0 - To print the available choices ");
        System.out.println("\t 1 - To print the grocery list ");
        System.out.println("\t 2 - To add an item ");
        System.out.println("\t 3 - To check if the item already exist on the list ");
        System.out.println("\t 4 - To modify an item ");
        System.out.println("\t 5 - To delete an item ");
        System.out.println("\t 6 - To quit ");
    }

    public static void addItem(){
        System.out.println("Please enter the name of the grocery item you want to add:");
        String addGroceryItem = s.next();
        groceryList.addGroceryItem(addGroceryItem);
    }

    public static void queryItem(){
        System.out.println("Please enter the name of the item you looking for:");
        String queryGroceryItem = s.next();
        groceryList.findGroceryItem(queryGroceryItem);
    }

    public static void modifyItem(){
        System.out.println("Please enter the item what you want to modify:");
        String oldGroceryItem = s.next();
        if (!groceryList.findGroceryItem(oldGroceryItem)){
            System.out.println("No such item on the list");
        }else {
            System.out.println("Please enter the new item:");
            String newGroceryItem = s.next();
            groceryList.modifyGroceryItem(oldGroceryItem,newGroceryItem);
        }
    }

    private static void deleteItem(){
        System.out.println("Please enter the name of the item you want to delete");
        String groceryItemToDelete = s.next();
        groceryList.removeGroceryItem(groceryItemToDelete);
    }
}

//        groceryList.addGroceryItem("Milk");
//        groceryList.addGroceryItem("Bread");
//        groceryList.addGroceryItem("Butter");
//        groceryList.addGroceryItem("Salt");

//        groceryList.printGroceryList();
//        groceryList.removeGroceryItem(1);
//        groceryList.printGroceryList();
//        groceryList.modifyGroceryItem(1,"Cauliflower");
//        groceryList.printGroceryList();

//        groceryList.findGroceryItem("SaLT");
//        groceryList.findGroceryItem("Tomato");
//        groceryList.getGroceryItemPosition("Salt");
//        groceryList.removeGroceryItem("Salt");
//        groceryList.printGroceryList();
//        groceryList.modifyGroceryItem("MiLk","Soj milk");
