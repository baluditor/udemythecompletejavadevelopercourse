package udemyTheCompleteJavaDeveloperCourse;

public class EnchantedPlayer {
    private String name;
    private int health = 100;
    private String weapon;

    public EnchantedPlayer(String name, int health, String weapon) {
        this.name = name;
        if(health > 0 && health < 100){
            this.health = health;
        }
        this.weapon = weapon;
    }

    public EnchantedPlayer(String name, String weapon){
        this.name = name;
        this.weapon = weapon;
    }

    public void loseHealth(int damage){
        this.health -= damage;
        if (health <= 0){
            System.out.println(name + " knocked out");
        }
    }

    public String  announceWeapon(){
        return this.weapon;
    }

    public int getHealth(){
        return this.health;
    }
}
