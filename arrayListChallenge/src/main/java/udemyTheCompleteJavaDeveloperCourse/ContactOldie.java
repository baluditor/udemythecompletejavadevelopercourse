package udemyTheCompleteJavaDeveloperCourse;

public class ContactOldie {
    private String name;
    private String phoneNumber;

    public ContactOldie(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public static ContactOldie createContact(String name, String phoneNumber){
        return new ContactOldie(name, phoneNumber);
    }



}
