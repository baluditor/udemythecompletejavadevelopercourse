package udemyTheCompleteJavaDeveloperCourse;

public class OopMasterChallengeRunner
{
    public static void main( String[] args )
    {
        Hamburger hamburger = new Hamburger("Basic","white","beef",300);
        hamburger.addAdditions1("lettuce",10);
        hamburger.addAdditions2("tomato",15);
        hamburger.addAdditions3("lettuce",20);
        hamburger.addAdditions4("lettuce",30);

        System.out.println("The price of the hamburger with "+hamburger.getAllAdditions()+" was "+hamburger.getTotalPrice());

        HealthyBurger healthyBurger = new HealthyBurger("pork",300);
        healthyBurger.addAdditions1("lettuce",10);
        healthyBurger.addAdditions2("tomato",10);
        healthyBurger.addAdditions3("lettuce",10);
        healthyBurger.addAdditions4("mozzarella",10);
        healthyBurger.addAdditions5("goat cheese",10);
        healthyBurger.addAdditions6("ketchup",10);

        System.out.println("The price of the hamburger with "+healthyBurger.getAllAdditions()+" was "+healthyBurger.getTotalPrice());

        DeluxeHamburger deluxeHamburger = new DeluxeHamburger("brown bread","steak", 400);
        System.out.println("The price of the hamburger with "+deluxeHamburger.getAllAdditions()+" was "+deluxeHamburger.getTotalPrice());
    }
}
