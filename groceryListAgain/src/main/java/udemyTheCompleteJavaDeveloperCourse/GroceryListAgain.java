package udemyTheCompleteJavaDeveloperCourse;

import java.util.ArrayList;

public class GroceryListAgain {
    private ArrayList groceryList = new ArrayList<String>();

    public void printGroceryList(){
        for (int i=0;i<groceryList.size(); i++){
            System.out.println("The "+(i+1)+". item on the list is "+groceryList.get(i).toString());
        }
    }

    public void addGroceryItem(String groceryItem){
        groceryList.add(groceryItem);
    }

    public void removeGroceryItem(int positionOfGroceryItem){//
        groceryList.remove(positionOfGroceryItem-1);
    }

    public void removeGroceryItem(String groceryItemName){
        if (getGroceryItem(groceryItemName)){
            int position = getGroceryItemPosition(groceryItemName);
            groceryList.remove(getGroceryItemPosition(groceryItemName));
            System.out.println(groceryItemName+" found at the "+ (position+1) +" and successfully removed");
        } else {
            System.out.println("No such item on the list");
        }
    }

    public void modifyGroceryItem(int positionOfGroceryItem, String newGroceryItem){
        String originalGroceryItem = groceryList.get(positionOfGroceryItem-1).toString();
        groceryList.set(positionOfGroceryItem-1,newGroceryItem);
        System.out.println("The "+positionOfGroceryItem+". item was modified from "+originalGroceryItem+" to "+newGroceryItem);
    }

    public void modifyGroceryItem(String oldGroceryItemName, String newGroceryItemName){
        int position = getGroceryItemPosition(oldGroceryItemName);
        if (getGroceryItem(oldGroceryItemName)){
            groceryList.set(getGroceryItemPosition(oldGroceryItemName),newGroceryItemName);
            System.out.println(oldGroceryItemName+" was found at the "+(position+1)+" place and been replaced with "+newGroceryItemName);
        } else {
            System.out.println("No such item on the list");
        }
    }

    private Boolean getGroceryItem(String groceryItemName){
        for (int i=0; i<groceryList.size(); i++){
            if (groceryList.get(i).toString().toLowerCase().equals(groceryItemName.toLowerCase())){
                //System.out.println("Item found");
                return true;
            }
        }
        //System.out.println("No such item on the list");
        return false;
    }

    public Boolean findGroceryItem(String groceryItemName){
        for (int i=0; i<groceryList.size(); i++){
            if (groceryList.get(i).toString().toLowerCase().equals(groceryItemName.toLowerCase())){
                System.out.println("Item found a the "+(i+1)+" place");
                return true;
            }
        }
        //System.out.println("No such item on the list");
        return false;
    }

    private int getGroceryItemPosition(String groceryItemName){
        for (int i=0; i<groceryList.size(); i++){
            if (groceryList.get(i).toString().toLowerCase().equals(groceryItemName.toLowerCase())){
                //System.out.println(groceryItemName+" found at the "+(i+1)+ " position");
                return i;
            }
        }
        System.out.println("No such item on the list");
        return -1;
    }

    //    public void printGroceryList(ArrayList groceryList){
//        groceryList = this.groceryList;
//        for (int i=0;i<groceryList.size(); i++){
//            System.out.println("The "+(i+1)+". item on the list is "+groceryList.get(i).toString());
//        }
//    }
}


