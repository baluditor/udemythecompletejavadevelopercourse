package udemyTheCompleteJavaDeveloperCourse;

import java.util.ArrayList;

public class Bank {
    private String name;
    private ArrayList<Branch> branch;

    public Bank(String name) {
        this.name = name;
        this.branch = new ArrayList<>();
    }

    public Boolean findBranchName(String branchName) {
        for (Branch b : this.branch) {
            if (b.getName().toLowerCase().equals(branchName.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public int findBranchNamePosition(String branchName) {
        for (int i=0; i<this.branch.size(); i++) {
            Branch branch = this.branch.get(i);
            if (branch.getName().toLowerCase().equals(branchName.toLowerCase())) {
                return i;
            }
        }
        return -1;
    }

    public void addNewBranch(Branch branch) {
        if (findBranchName(branch.getName())) {
            System.out.println("Branch already exist with name "+branch.getName());
        } else {
            System.out.println("Branch with " + branch.getName() + " added successfully");
            this.branch.add(branch);
        }
    }

    public void addNewCustomerToBranch(String branchName, String customerName, double initialTransactionAmount) {
        Customer customer = new Customer(customerName, initialTransactionAmount);
        if (findBranchName(branchName)) {
            if (!branch.get(findBranchNamePosition(branchName)).findCustomerName(customerName)) {
                branch.get(findBranchNamePosition(branchName)).addCustomer(customer);
                System.out.println("Customer "+customerName+" added to the "+branchName+" branch with an initial transaction of "+initialTransactionAmount+"$");
            } else {
                System.out.println("Customer with name "+customerName+" in a branch "+branchName+" already exist");
            }
        } else {
            System.out.println("Branch with name "+branchName+" does not exist ");
        }
    }

    public void addTransactionToCustomer(String branchName, String customerName, Double amount){
        if (findBranchName(branchName)) {
            if (this.branch.get(findBranchNamePosition(branchName)).findCustomerName(customerName)) {
                this.branch.get(findBranchNamePosition(branchName)).getCustomer().get(this.branch.get(findBranchNamePosition(branchName)).findCustomerPosition(customerName)).addTransactions(amount);
                System.out.println(amount+"$ added to customer "+customerName+" at branch "+branchName);
            } else {
                System.out.println("No customer with name " + customerName + " exist");
            }
        }else {
            System.out.println("No branch with "+branchName+" name exist");
        }
    }

    public void listBranchesNames(){
        for (int i=0; i<branch.size(); i++) {
            System.out.println(i+1+". branch is "+branch.get(i).getName());
        }
    }


    public void printCustomersInBranchWithTheirTransactions(String branchName){
        if (findBranchName(branchName)){
            for (int i=0; i<this.branch.get(findBranchNamePosition(branchName)).getCustomer().size(); i++) {
                String customerName = this.branch.get(findBranchNamePosition(branchName)).getCustomer().get(i).getName();
                Customer c = this.branch.get(findBranchNamePosition(branchName)).getCustomer().get(i);
                System.out.println(i+1 +". Customer's name: "+customerName);
                System.out.println("Customer "+customerName+" actual balance: "+c.balance());
                c.printTransactions();
            }
        } else {
            System.out.println("No branch with "+branchName+" name");
        }
    }
}
