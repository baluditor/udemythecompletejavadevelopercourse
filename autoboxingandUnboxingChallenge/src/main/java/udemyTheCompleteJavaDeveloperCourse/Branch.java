package udemyTheCompleteJavaDeveloperCourse;

import java.util.ArrayList;

public class Branch {
    private String name;
    private ArrayList<Customer> customer;

    public Branch(String name) {
        this.name = name;
        this.customer = new ArrayList<>();
    }

    public void addCustomer(Customer customer) {
        if (findCustomer(customer)>=0) {
            System.out.println("Customer already exist");
        }
        this.customer.add(customer);
    }

    public int findCustomer(Customer customer){
        return this.customer.indexOf(customer);
    }

    public int findCustomerPosition(String customerName) {
        for (int i = 0; i < this.customer.size(); i++) {
            Customer customer = this.customer.get(i);
            if (customer.getName().toLowerCase().equals(customerName.toLowerCase())) {
                return i;
            }
        }
        return -1;
    }
    public boolean findCustomerName(String customerName){
        for (int i=0; i<this.customer.size(); i++) {
            if (this.customer.get(i).getName().toLowerCase().equals(customerName.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Customer> getCustomer() {
        return customer;
    }
}
