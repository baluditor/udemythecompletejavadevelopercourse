package udemyTheCompleteJavaDeveloperCourse;

public class Room {
    private Wall wall1;
    private Wall wall2;
    private Wall wall3;
    private Wall wall4;
    private Wall floor;
    private Wall ceiling;
    private Window window;
    private Dimensions dimensions;
    private Lamp lamp;
    private Door door;

    public Room(Wall wall1, Wall wall2, Wall wall3, Wall wall4, Wall floor, Wall ceiling, Window window, Dimensions dimensions, Lamp lamp, Door door) {
        this.wall1 = wall1;
        this.wall2 = wall2;
        this.wall3 = wall3;
        this.wall4 = wall4;
        this.floor = floor;
        this.ceiling = ceiling;
        this.window = window;
        this.dimensions = dimensions;
        this.lamp = lamp;
        this.door = door;
    }

    public void windowStatusChanger(){
        changeWindowstatus();
    }

    private void changeWindowstatus(){
        window.chageWindowstatus();
    }

    public Wall getWall() {
        return wall1;
    }

    private Window getWindow() {
        return window;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public Lamp getLamp() {
        return lamp;
    }

    public Door getDoor() {
        return door;
    }

    public Wall getWall1() {
        return wall1;
    }

    public Wall getWall2() {
        return wall2;
    }

    public Wall getWall3() {
        return wall3;
    }

    public Wall getWall4() {
        return wall4;
    }

    public Wall getFloor() {
        return floor;
    }

    public Wall getCeiling() {
        return ceiling;
    }
}
