package udemyTheCompleteJavaDeveloperCourse;

import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class App {

//    static class intClass{
//        private int myInt;
//
//        public intClass(int myInt) {
//            this.myInt = myInt;
//        }
//
//        public int getMyInt() {
//            return myInt;
//        }
//
//        public void setMyInt(int myInt) {
//            this.myInt = myInt;
//        }
//    }

    public static void main( String[] args ) {
//        String[] myStringArray = new String[10];
//        int[] myIntArray = new int[10];
//
//        ArrayList<String> myStringArrayList = new ArrayList<>();
//        myStringArrayList.add("Balu");
//
//        ArrayList<intClass> intClassArrayList = new ArrayList<>();
//        intClassArrayList.add(new intClass(60));

//        ArrayList<Integer> integers = new ArrayList<>();
//        for (int i=0; i<=10; i++){
//            integers.add(Integer.valueOf(i));
//        }
//        for (int i=0; i<=10; i++) {
//            System.out.println(i + " ---> " + integers.get(i).intValue());
//        }
//
//        Integer myIntValue = 50; //Integer.valueOf(50);
//        int myInt = myIntValue; //myIntValue.intValue();

        ArrayList<Double> myDoubleArrayList = new ArrayList<>();

        for (double dbl =0.0; dbl<=20.0; dbl += 0.5){
            myDoubleArrayList.add(dbl);
        }

        for (int i=0; i<myDoubleArrayList.size(); i++){
            System.out.println(i+" ---> "+myDoubleArrayList.get(i));
        }

    }
}
