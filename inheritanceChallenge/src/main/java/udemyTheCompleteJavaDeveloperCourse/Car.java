package udemyTheCompleteJavaDeveloperCourse;

public abstract class Car extends Vehicle {

    private int doors;
    private int windows;
    private int wheels;
    private boolean isAutomatic;
    private String propulsion;
    private int speed = 0;
    private String direction = null;
    private int degree = 0;
    private int gear =0;

    Car(){

    }

    public Car(int weight, int height, int width, int length, String color, int doors, int windows, int wheels, boolean isAutomatic, String propulsion, int speed, String direction, int degree, int gear) {
        super(weight, height, width, length, color);
        this.doors = doors;
        this.windows = windows;
        this.wheels = wheels;
        this.isAutomatic = isAutomatic;
        this.propulsion = propulsion;
        this.speed = 0;
        this.direction = null;
        this.degree = 0;
        this.gear = 0;
    }

    public Car(int weight, int height, int width, int length, String color, int doors, int windows, int wheels, boolean isAutomatic, String propulsion) {
        super(weight, height, width, length, color);
        this.doors = doors;
        this.windows = windows;
        this.wheels = wheels;
        this.isAutomatic = isAutomatic;
        this.propulsion = propulsion;
    }

    public void steering(String direction,int degree){
        this.degree = degree;
        System.out.println("Car changing direction to "+direction+" with "+degree+"degree");
    }

    public void increaseSpeed(int speed){
        this.speed += speed;
        System.out.println("Car's speed increase with "+speed+" current speed is "+ this.speed);
    }

    public void decreaseSpeed(int speed){
        this.speed -= speed;
        System.out.println("Car's speed decrease with "+speed+" current speed is "+ this.speed);
    }

    public void status(){
        System.out.println("The car is going with "+this.speed+" km/h to the "+this.direction+" with a degree of "+this.degree+" and change gear to "+changeGear());
    }

    private int changeGear(){
        if (speed == 0){
            this.gear = 0;
        } else if (speed>0 && speed<11){
            this.gear=1;
        } else if (speed>=11 && speed<21){
            this.gear=2;
        } else if (speed>=21 && speed<31){
            this.gear=3;
        } else if (speed>=31 && speed<41){
            this.gear=4;
        } else if (speed>=41 && speed<51){
            this.gear=5;
        } else {
            this.gear=6;
        }
        return this.gear;
    }

    public int getDoors() {
        return doors;
    }

    public int getWindows() {
        return windows;
    }

    public int getWheels() {
        return wheels;
    }

    public boolean isAutomatic() {
        return isAutomatic;
    }

    public String getPropulsion() {
        return propulsion;
    }

    public int getSpeed() {
        return speed;
    }

    public String getDirection() {
        return direction;
    }

    public int getDegree() {
        return degree;
    }

    public int getGear() {
        return gear;
    }
}
