package udemyTheCompleteJavaDeveloperCourse;

import java.util.ArrayList;

public class Customer {
    private String name;
    private ArrayList<Double> transactions;

    public Customer(String name, double initialTransactionAmount) {
        this.name = name;
        this.transactions = new ArrayList<>();
        transactions.add(initialTransactionAmount);
    }

    public void printTransactions() {
        for (int i=0; i<transactions.size(); i++){
            if (i == 0) {
                System.out.println("Initial transaction: " + transactions.get(0));
            } else {
                System.out.println(i + ". transaction was " + transactions.get(i));
            }
        }
    }

    public void addTransactions(double amount){
        this.transactions.add(amount);
    }

    public String getName() {
        return name;
    }

    public double balance() {
        double balance = 0;
        for (int i=0; i<transactions.size(); i++){
            balance +=transactions.get(i);
        }
        return balance;
    }
}