package udemyTheCompleteJavaDeveloperCourse;

class Movie{
    private String name;

    public Movie(String name){
        this.name = name;
    }

    public String plot(){
        return "No plot here";
    }

    public String getName(){
        return this.name;
    }
}

class Jaws extends Movie{
    public Jaws(){
        super("Jaws");
    }
    @Override
    public String plot(){
        return "Shark eating a lot of ppl";
    }
}

class IndependenceDay extends Movie{
    public IndependenceDay(){
        super("Independence day");
    }

    @Override
    public String plot(){
        return "Aliens trying to overtake the earth.";
    }
}

class StarWars extends Movie{
    public StarWars(){
        super("Star Wars");
    }
    @Override
    public String plot(){
        return "Pew pew pew";
    }
}

class MazeRunner extends Movie{
    public MazeRunner(){
        super("Maserunner");
    }

    @Override
    public String plot(){
        return "Run run run kids";
    }
}

class Forgetable extends Movie{
    public Forgetable(){
        super("I can't even recall");
    }

    // no plot as I can not fcking remember
}


public class Main {
    public static void main( String[] args ){

        for (int i =1; i<11; i++){
            Movie movie = randomMovie();
            System.out.println("Movie #"+i+": "+movie.getName()+"\n"+"Plot: "+movie.plot()+"\n");
        }

    }
    public static Movie randomMovie(){
        int randomNumber = (int) (Math.random()*5)+1;
        System.out.println("The random number generated was "+randomNumber);
        switch (randomNumber){
            case 1:
                return new Jaws();
            case 2:
                return new IndependenceDay();
            case 3:
                return new StarWars();
            case 4:
                return new MazeRunner();
            case 5:
                return new Forgetable();
        }
        return null;
    }
}

