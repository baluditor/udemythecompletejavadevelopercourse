package udemyTheCompleteJavaDeveloperCourse;

import java.util.ArrayList;

public class GroceryListRunner {

    private static GroceryList groceryList = new GroceryList();

    public static void main(String[] args) {

        groceryList.printGroceryList();
        groceryList.addGroceryItem("First item");
        groceryList.addGroceryItem("Second item");
        groceryList.addGroceryItem("Third item");
        groceryList.addGroceryItem("Forth item");

        groceryList.printGroceryList();

        groceryList.modifyGroceryItem(4, "New forth item");

        String item = "First item";
        groceryList.removeGroceryItem(item);
        groceryList.printGroceryList();
        searchForItem("Third item");
    }

    public static void searchForItem(String searchItem) {
        if(groceryList.onFile(searchItem)) {
            System.out.println("Found " + searchItem);
        } else {
            System.out.println(searchItem + ", not on file.");
        }
    }

//    public static void processedArrayList(){
//        ArrayList<String> newArray = new ArrayList<String>();
//        ArrayList<String> newArray2 = new ArrayList<String>();
//        newArray.addAll(groceryList.getGroceryList());
//    }

    public static void processArrayList() {
        ArrayList<String> newArray = new ArrayList<>();
        newArray.addAll(groceryList.getGroceryList());

        ArrayList<String> nextArray = new ArrayList<>(groceryList.getGroceryList());

        String[] myArray = new String[groceryList.getGroceryList().size()];
        myArray = groceryList.getGroceryList().toArray(myArray);


    }

}

