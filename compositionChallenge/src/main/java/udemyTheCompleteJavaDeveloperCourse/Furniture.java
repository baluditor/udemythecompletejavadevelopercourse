package udemyTheCompleteJavaDeveloperCourse;

public abstract class Furniture {
    private Dimensions dimensions;
    private PhysicalAttributes physicalAttributes;

    public Furniture(Dimensions dimensions, PhysicalAttributes physicalAttributes) {
        this.dimensions = dimensions;
        this.physicalAttributes = physicalAttributes;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public PhysicalAttributes getPhysicalAttributes() {
        return physicalAttributes;
    }
}
