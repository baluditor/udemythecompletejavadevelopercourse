package udemyTheCompleteJavaDeveloperCourse;

import java.util.ArrayList;

public class Inventory {
    private ArrayList<Product> inventory;

    public Inventory() {
        this.inventory = new ArrayList<>();
    }

    public Boolean addProduct(Product product){
        if (findProduct(product)>=0){
            return false;
        } else {
            inventory.add(product);
            return true;
        }
    }

    private int findProduct(Product product){
        return inventory.indexOf(product);
    }

    public int findProduct(String productName){
        for (int i=0; i<this.inventory.size(); i++){
            Product product = this.inventory.get(i);
            if (productName.toLowerCase().equals(product.getModel().toLowerCase())){
                return i;
            }
        }
        return -1;
    }

    public boolean deleteProduct(Product product){
       int productFoundAtPosition = findProduct(product);
       if (productFoundAtPosition<0){
            return false;
       }
       this.inventory.remove(productFoundAtPosition);
       return true;
    }

    public Product queryProduct(String product){
        int productFoundAtPosition = findProduct(product);
        if (productFoundAtPosition>=0){
            return this.inventory.get(productFoundAtPosition);
        }
        return null;
    }

    public void modifyQuantity(Product product, int pieces){
        int productFoundAtPosition = findProduct(product);
        if (productFoundAtPosition>=0){
            this.inventory.get(productFoundAtPosition).addPieces(pieces);
        }
    }
    public void printInventory() {
        for (int i = 0; i < this.inventory.size(); i++) {
            System.out.println("ID: "
                    + (i+1) + "\nBrand:" + this.inventory.get(i).getBrand() +
                    "\n Type: " + this.inventory.get(i).getType() +
                    "\n Type: " + this.inventory.get(i).getModel() +
                    "\n Type: " + this.inventory.get(i).getColor() +
                    "\n Type: " + this.inventory.get(i).getPrice() +
                    "\n Type: " + this.inventory.get(i).getPiece()
            );
        }
    }
}
