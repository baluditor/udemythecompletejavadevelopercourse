package udemyTheCompleteJavaDeveloperCourse;

public class Arrays{
    public static void main( String[] args ){
        int[] myIntArray = new int[10];
        int[] mySecondIntArray = {1,2,3,4,5,6,7,8,9,10};
        myIntArray[5] = 60;
        double[] myDoubpeArray = new double[15];
        myDoubpeArray[0] = 1.5;

        System.out.println(myDoubpeArray[0]);
        System.out.println(myIntArray[5]);
        System.out.println(mySecondIntArray[9]);

        int[] myForLoopIntArray = new int[25];

        for (int i=0; i<myForLoopIntArray.length; i++){
            myForLoopIntArray[i] =i*10;
            System.out.println("Element "+i+" of our myForLoopIntArray is "+myForLoopIntArray[i]);
        }

        printArray(mySecondIntArray);
        printArray(myIntArray);
    }

    public static void printArray(int[] arrayName){
        for (int i=0; i<arrayName.length; i++){
            System.out.println("Element "+i+" of our myForLoopIntArray is "+arrayName[i]);
        }
    }
}
