package udemyTheCompleteJavaDeveloperCourse;

public class Lamp extends Furniture {
    private boolean isOn;

    public Lamp(Dimensions dimensions, PhysicalAttributes physicalAttributes) {
        super(dimensions, physicalAttributes);
        this.isOn = false;
    }

    public void turnOnLamp(){
        isOn = true;
        System.out.println("The lamp has been turned on");
    }

    public void turnOffLamp(){
        isOn = false;
        System.out.println("The lamp has been turned off");
    }

    public void isOn() {
        if (isOn){
            System.out.println("The lamp is on");
        } else {
            System.out.println("This lamp is off");
        }
    }
}
