package udemyTheCompleteJavaDeveloperCourse;

public class Honda extends Car {

    private String type;

    public Honda(){

    }

    public Honda(String type,int weight, int height, int width, int length, String color, int doors, int windows,boolean isAutomatic, String propulsion){
        super(weight,height,width,length,color,doors,windows,4,isAutomatic,propulsion,0,null,0,0);
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
