package udemyTheCompleteJavaDeveloperCourse;

public class RunnerClass
{
    public static void main( String[] args )
    {

        Honda civic = new Honda("CRX",1500,140,100,240,"black",4,4,false,"front");
        System.out.println(civic.getType());
        System.out.println(civic.getColor());
        civic.increaseSpeed(15);
        civic.steering("left", 20);
        civic.status();
        civic.increaseSpeed(10);
        civic.increaseSpeed(10);
        civic.increaseSpeed(10);
        civic.increaseSpeed(10);
        civic.decreaseSpeed(40);
        civic.decreaseSpeed(10);
        civic.status();


    }
}
