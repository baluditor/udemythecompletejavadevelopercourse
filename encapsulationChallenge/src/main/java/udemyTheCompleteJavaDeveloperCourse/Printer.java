package udemyTheCompleteJavaDeveloperCourse;

public class Printer {
    private int tonerLevel = 100;
    private int papersPrinted;
    private boolean isDuplex;

    public void print(int pages, boolean duplexPrinting){
        if (this.tonerLevel-pages<0){
            System.out.println("Can't print "+pages+" pages as the current toner level is "+this.tonerLevel+"%. With that amount of toner you can print only "+this.tonerLevel+" pages");
        } else if (isDuplex && duplexPrinting){
            this.papersPrinted += pages/2+1;
            this.tonerLevel -= pages;
            System.out.println(pages+" has been printed now. Total printed pages "+this.papersPrinted+". The toner level decrease to "+this.tonerLevel+"%");

        } else {
            this.papersPrinted += pages;
            this.tonerLevel -= pages;
            System.out.println(pages+" has been printed now. Total printed pages "+this.papersPrinted+". The toner level decrease to "+this.tonerLevel+"%");
        }
    }

    public void fillToner(int level){
        if ((this.tonerLevel + level) >100){
            System.out.println("Can't fill the toner with "+level+"%, it would overflow, as the current level of the toner is "+this.tonerLevel+"%");
        } else {
            this.tonerLevel += level;
        }
    }


    public Printer(boolean isDuplex) {
        this.isDuplex = isDuplex;
    }


}
